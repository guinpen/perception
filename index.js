const { app, BrowserWindow } = require( 'electron' )
var timeAFK;
app.on( 'ready', () => {
  let myWindow = new BrowserWindow({
    width: 1080,
    height: 700,
    webPreferences: {
      nodeIntegration: true,
    },
  })
  myWindow.setMenuBarVisibility(false)
  myWindow.loadFile('index.html')
});
