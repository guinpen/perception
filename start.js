function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function changePage() {
    await sleep(5000);
    window.location.replace("home.html");
}

changePage();
