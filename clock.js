
function startTime() {

    var st = document.getElementById("startTime");
    var et = document.getElementById("endTime");
    var lt = document.getElementById("lunchTime");
    var lte = document.getElementById("lunchTimeEnd");
    var but = document.getElementById("save");

    but.addEventListener("click", function() {

      var today = new Date();
      var h = today.getHours();
      var m = today.getMinutes();

      var startTimeRaw = st.value;
      var startTimes = startTimeRaw.split(":");
      var startHr = parseInt(startTimes[0]);
      var startMin = parseInt(startTimes[1]);

      var endTimeRaw = et.value;
      var endTimes =  endTimeRaw.split(":");
      var endHr = parseInt(endTimes[0]);
      var endMin = parseInt(endTimes[1]);

      var lunchTimeRaw = lt.value;
      var lunchTimes = lunchTimeRaw.split(":");
      var lunchTimeStartHr = parseInt(lunchTimes[0]);
      var lunchTimeStartMin = parseInt(lunchTimes[1]);

      var lunchTimeRaw2 = lte.value;
      var lunchTimes2 = lunchTimeRaw2.split(":");
      var lunchTimeEndHr = parseInt(lunchTimes2[0]);
      var lunchTimeEndMin = parseInt(lunchTimes2[1]);


        if ((h*60 + m) > (startHr*60 + startMin) && (h*60 + m) < (endHr*60 + endMin)) {
          if (((h*60 + m) < lunchTimeStartHr*60+lunchTimeStartMin) || ((h*60 + m)  > lunchTimeEndHr*60+lunchTimeEndMin)) {
            var totalTime = (endHr*60 + endMin) - (h*60 + m);
            var hour = Math.floor(totalTime/60);
            var minute = totalTime%60;
            minute = checkTime(minute);
            document.getElementById('clock').innerHTML = hour + " hours " + minute + " minutes";
          } else {
            document.getElementById('clock').innerHTML = "Take your lunch break!";
          }
        } else {
          if ((h*60 + m) < (startHr*60 + startMin)) {
            document.getElementById('clock').innerHTML = "Going to be a productive day!";
          } else {
            document.getElementById('clock').innerHTML = "All in a day's work";
          }

        }

    }, false);
}
function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}
